<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liquor Store</title>
    <style>
    	body, h1, h2, h3, h4, h5, h6{
    		font-family: Arial, san-serif;
    	}
    </style>
</head>
<body>
<center>
<h1>
    Available Brands
</h1>
<%
List result= (List) request.getAttribute("brands");
Iterator it = result.iterator();
out.println("<br>At LCBO, We have <br><br>");
while(it.hasNext()){
out.println(it.next()+"<br>");

}
%>
<script>
function goBack() {
  window.history.back();
}
</script>

<button onclick="goBack()">Go Back</button>
</body>
</html>